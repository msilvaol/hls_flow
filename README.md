# HLS Flow

Clone the repository:
```
git clone ssh://git@gitlab.cern.ch:7999/msilvaol/hls_flow.git
cd hls_flow
```

Use the following setup script for both eclipse and catapult design flows
```
source /eda/Siemens/2021-22/scripts/CATAPULT_2021.1_RHELx86.sh
export "CPLUS_INCLUDE_PATH=$MGC_HOME/shared/include/"
export "CPLUS_INCLUDE_PATH=$CPLUS_INCLUDE_PATH:$MGC_HOME/pkgs/siflibs/"
export "CPLUS_INCLUDE_PATH=$CPLUS_INCLUDE_PATH:$MGC_HOME/pkgs/hls_pkgs/mgc_comps_src/"
export "LIBRARY_PATH=$MGC_HOME/shared/lib/"
export "LIBRARY_PATH=$LIBRARY_PATH:$MGC_HOME/shared/lib/Linux/gcc"
export PATH=/home/sw/eclipse:$PATH
```

## Eclipse (Automatic)

Open eclipse with the workpsace of preference. Then clone the git repository using the following steps: 

1) File >> Import >> Git >> Projects from Git >> Next >> Clone URI >> Next
2) Enter the "Clone with SSH" URI of this git repository in the URI field and click Next twice
3) Select the directory where the repository should be located (this is not yet the location of the eclipse project), and click Next twice
4) Review the eclipse projects being imported and click Finish
5) Press The Run button and check if the output is as follows:

```
../src/fir_filter_tb.cpp:113 - Read in 127 coefficients from '../data/coefficients.csv'
../src/fir_filter_tb.cpp:151 - CSV file '../data/samples.csv' 256 samples were read in.
../src/fir_filter_tb.cpp:165 - Writing output csv file to '../data/filter_output.csv'.
../src/fir_filter_tb.cpp:74 - End of testbench.
```


## Eclipse (Manual)


1) Open eclipse with the workspace of your preference or create a new one (recomended)
2) File >> New >> C/C++ Project >> C++ Managed Build
3) Click Next
4) Type `filter_hls` in the field Project name
5) Uncheck option "Use default location"
6) Click Browse and select the `hls_flow/eclipse` location
7) Under Project Type, select Executable >> Empty Project
8) Under Toolchains, select Linux GCC
9) Click Finish 
10) Close the welcome window
11) Project >> Buil All (Ctrl+B)
12) Run >> Run configurations... >> C/C++ Application (double click)
13) Click Run 



## Catapult

```
cd catapult
catapult -f setup.tcl
```

## Creating an eclipse project to be stored in git

1) Create a new C++ project (empty and Linux GCC) using steps 1-4 from Eclipse (Manual) section
2) Keep the default location option, i.e. the project will created in your workspace
3) Click Finish
4) Ricght-click on project >> Team >> Shared Project >> Select where the repository is cloned 
5) Use the option Path within repository in case the project folder should be located in a given path of the repository 
6) Click Finish
7) Go the project folder in the terminal and create symbolic links to the folders the project requires access, e.g. source and data folders 
8) Build the project using (Ctrl + B)
9) Run >> Run configurations... >> C/C++ Application (double click)
10) At the Common tab, select the options Shared file in the Save as section
11) Click Apply, and then Run

