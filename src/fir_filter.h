//
// Copyright 2018-2021 Siemens
//

#ifndef FIR_FILTER_H_
#define FIR_FILTER_H_

#include <ac_fixed.h>

const unsigned FILTER_ORDER = 126;         // filter order
const unsigned TAP_COUNT = FILTER_ORDER+1; // there are N+1 coefficients for an N-order filter

typedef ac_fixed<3,2,true>   X_TYPE;
typedef ac_fixed<10,0,true> COEFF_TYPE;
typedef ac_fixed<9,1,true,AC_RND,AC_SAT_SYM> Y_TYPE;

void fir_filter(const X_TYPE, COEFF_TYPE [TAP_COUNT], Y_TYPE &);
#endif
