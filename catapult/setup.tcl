//  Catapult Ultra Synthesis 2021.1/950854 (Production Release) Mon Aug  2 21:36:02 PDT 2021
//  
//          Copyright (c) Siemens EDA, 1996-2021, All Rights Reserved.
//                        UNPUBLISHED, LICENSED SOFTWARE.
//             CONFIDENTIAL AND PROPRIETARY INFORMATION WHICH IS THE
//                   PROPERTY OF SIEMENS EDA OR ITS LICENSORS.
//  
//  Running on Linux msilvaol@mes 3.10.0-1160.66.1.el7.x86_64 x86_64 aol
//  
//  Package information: SIFLIBS v23.7_0.0, HLS_PKGS v23.7_0.0, 
//                       SIF_TOOLKITS v23.7_0.0, SIF_XILINX v23.7_0.0, 
//                       SIF_ALTERA v23.7_0.0, CCS_LIBS v23.7_0.0, 
//                       CDS_PPRO v10.5a, CDS_DesigChecker v2021.1, 
//                       CDS_OASYS v20.1_3.6, CDS_PSR v20.2_1.8, 
//                       DesignPad v2.78_1.0
//  
//  Start time Wed Aug  3 16:34:43 2022
project new -name Catapult_project
solution file add ../src/fir_filter_tb.cpp -exclude true
solution file add ../src/csvparser.cpp -exclude true
solution file add ../src/fir_filter.h -exclude true
solution file add ../src/fir_filter.cpp
solution file add ../src/csvparser.h -exclude true
go analyze
solution design set fir_filter -top
go compile
solution library add mgc_Altera-Cyclone-V-6_beh -- -rtlsyntool Quartus -manufacturer Altera -family {Cyclone V} -speed 6 -part 5CEBA2F17C6
solution library add Altera_DIST
solution library add Altera_M10K
solution library add Altera_MLAB
go libraries
directive set -CLOCKS {clk {-CLOCK_PERIOD 10 -CLOCK_EDGE rising -CLOCK_HIGH_TIME 5 -CLOCK_OFFSET 0.000000 -CLOCK_UNCERTAINTY 0.0 -RESET_KIND sync -RESET_SYNC_NAME rst -RESET_SYNC_ACTIVE high -RESET_ASYNC_NAME arst_n -RESET_ASYNC_ACTIVE low -ENABLE_NAME {} -ENABLE_ACTIVE high}}


